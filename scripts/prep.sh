#!/bin/bash
#SBATCH --partition=amdgpufast
#SBATCH --time=04:00:00
#SBATCH --nodes=1 --ntasks-per-node=1 --cpus-per-task=4
#SBATCH --gres=gpu:1
#SBATCH --mem=20G
#SBATCH --error=../../logs/fasttextHDFS1.%j.err
#SBATCH --out=../../logs/fasttextHDFS1.%j.out

# clear the environment from any previously loaded modules
ml purge > /dev/null 2>&1

ml PyTorch/1.7.1-fosscuda-2020b
source ../../../ml4logs_env/bin/activate

cd ../src/data

python prepare_hdfs.py 1