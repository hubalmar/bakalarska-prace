#!/bin/bash
#SBATCH --partition=gpu
#SBATCH --time=4:00:00
#SBATCH --nodes=1 --ntasks-per-node=1 --cpus-per-task=1
#SBATCH --mem=40G
#SBATCH --gres=gpu:1
#SBATCH --error=../../logs/fasttextHDFS1.%j.err
#SBATCH --out=../../logs/fasttextHDFS1.%j.out

# clear the environment from any previously loaded modules
ml purge > /dev/null 2>&1

FASTTEXT=~/fastText/./fasttext
DATADIR=../data/interim/HDFS1
OUTPUTDIR=../models/embeddings/remove
HIDDEN=8

DIR="$OUTPUTDIR/"
if [ ! -d "$DIR" ];
then
  mkdir "$DIR"
fi


DIR="$OUTPUTDIR/hidden-$HIDDEN/"
if [ ! -d "$DIR" ];
then
  mkdir "$DIR"
fi


# train FastText on all folds
ml GCC
for i in $(seq 1 $1)
do
    if [ ! -d "$DIR/$i/reformed" ];
    then
        mkdir "$DIR/$i/reformed"
    fi
    ${FASTTEXT} skipgram -input ${DATADIR}/${HIDDEN}-hidden/${i}/remove/remove-train-data-HDFS1-cv1-1.log -output ${OUTPUTDIR}/hidden-${HIDDEN}/${i}/reformed/fasttext-skipgram-hdfs1-d100-n3-6-cv1-1 -dim 100 -minn 3 -maxn 6 -minCount 10000 -thread 1
done
