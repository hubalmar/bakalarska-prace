#!/bin/bash
#SBATCH --partition=gpulong
#SBATCH --time=72:00:00
#SBATCH --nodes=1 --ntasks-per-node=1 --cpus-per-task=4
#SBATCH --mem=100G
#SBATCH --gres=gpu:1
#SBATCH --error=../../logs/change_repHDFS1.%j.err
#SBATCH --out=../../logs/change_repHDFS1.%j.out

# clear the environment from any previously loaded modules
ml purge > /dev/null 2>&1

ml PyTorch/1.7.1-fosscuda-2020b
source ../../../ml4logs_env/bin/activate

# train TCN model and evaluate hyperparameters on random search
cd /home/hubalmar/baka/methods4logfiles/src/experiments
python changed_replicate.py