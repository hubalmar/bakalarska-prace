#!/bin/bash
#SBATCH --partition=amdgpu
#SBATCH --time=24:00:00
#SBATCH --nodes=1 --ntasks-per-node=1 --cpus-per-task=4
#SBATCH --mem=100G
#SBATCH --gres=gpu:1
#SBATCH --error=../../logs/repHDFS1.%j.err
#SBATCH --out=../../logs/repHDFS1.%j.out

# clear the environment from any previously loaded modules
ml purge > /dev/null 2>&1

ml PyTorch/1.7.1-fosscuda-2020b
source ../../../ml4logs_env/bin/activate

# train TCN model and evaluate hyperparameters on random search
cd /home/hubalmar/baka/methods4logfiles/src/experiments
python replicate2.py