from drain3 import TemplateMiner
from collections import defaultdict
import pickle
from typing import List, Dict, DefaultDict, Union, Any, Tuple


def get_log_templates(clusters: List) -> Dict:
    ret = {cl.cluster_id: ' '.join(cl.log_template_tokens) for cl in clusters}
    return ret


def get_log_structure(log_lines: DefaultDict, cluster_ids: DefaultDict, clusters: List) -> Dict:
    templates = get_log_templates(clusters)

    ret_log_structure = {key: [] for key in log_lines.keys()}
    for key in ret_log_structure.keys():
        for curr_id, log in zip(cluster_ids[key], log_lines[key]):
            ret_log_structure[key].append((curr_id, log, templates[curr_id]))

    return ret_log_structure


def parse_file_drain3(data: DefaultDict, template_miner: TemplateMiner) -> Dict:
    # template_miner = TemplateMiner()

    cluster_ids = defaultdict(list)
    log_lines = defaultdict(list)
    for block_id, logs in data.items():
        for log in logs:
            line = log.rstrip().partition(': ')[2]  # produces tuple (pre, delimiter, post)
            result = template_miner.add_log_message(line)
            cluster_ids[block_id].append(result['cluster_id'])
            log_lines[block_id].append(line)

    log_structure = get_log_structure(log_lines, cluster_ids, template_miner.drain.clusters)
    return log_structure


def change_train_data_remove(data: DefaultDict, hide: List[int]) -> Dict:
    template_miner = TemplateMiner()
    train_data = defaultdict(list)
    for block_id, logs in data.items():
        for log in logs:
            line = log.rstrip().partition(': ')[2]  # produces tuple (pre, delimiter, post)
            result = template_miner.add_log_message(line)['cluster_id']
            if result not in hide:
                train_data[block_id].append(log)
    return train_data


def change_train_data_BOW(data: DefaultDict,
                          hide: List[int]) -> tuple:
    template_miner_replace = TemplateMiner()
    template_miner_remove = TemplateMiner()
    template_miner = TemplateMiner()
    cluster_ids = defaultdict(list)
    cluster_ids_remove = defaultdict(list)
    log_lines_remove = defaultdict(list)
    log_lines = defaultdict(list)
    for block_id, logs in data.items():
        for log in logs:
            line = log.rstrip().partition(': ')[2]  # produces tuple (pre, delimiter, post)
            result = template_miner.add_log_message(line)['cluster_id']
            if result in hide:
                line = log[:13] + " others: unknown log " + block_id
                # log = log.rstrip().partition(': ')[0] + log.rstrip().partition(': ')[1] + line + '\n'
            else:
                result = template_miner_remove.add_log_message(line)
                cluster_ids_remove[block_id].append(result['cluster_id'])
                log_lines_remove[block_id].append(line)
            result_replace = template_miner_replace.add_log_message(line)
            cluster_ids[block_id].append(result_replace['cluster_id'])
            log_lines[block_id].append(line)
    log_structure = get_log_structure(log_lines, cluster_ids, template_miner_replace.drain.clusters)
    log_structure_remove = get_log_structure(log_lines_remove, cluster_ids_remove, template_miner_remove.drain.clusters)
    return log_structure, log_structure_remove


def get_data_for_fasttext(data: DefaultDict, test_data: DefaultDict,
                          hide: List[int]) -> List[Dict]:

    replace_test_data = defaultdict(list)
    template_miner = TemplateMiner()
    train_data = defaultdict(list)
    for block_id, logs in data.items():
        for log in logs:
            line = log.rstrip().partition(': ')[2]  # produces tuple (pre, delimiter, post)
            result = template_miner.add_log_message(line)['cluster_id']
            if result in hide:
                line = log[:13] + " others: unknown log " + block_id
                log = line + '\n'
                # log = log.rstrip().partition(': ')[0] + log.rstrip().partition(': ')[1] + line + '\n'
            train_data[block_id].append(log)
    for block_id, logs in test_data.items():
        for log in logs:
            line = log.rstrip().partition(': ')[2]  # produces tuple (pre, delimiter, post)
            result = template_miner.add_log_message(line)['cluster_id']
            if result in hide:
                line = log[:13] + " others: unknown log " + block_id
                log = line + '\n'
                # log = log.rstrip().partition(': ')[0] + log.rstrip().partition(': ')[1] + line + '\n'
            replace_test_data[block_id].append(log)
    return [train_data, replace_test_data]


def save_drain3_to_file(data: Dict, file_path: str):
    with open(file_path, 'wb') as f:
        pickle.dump(data, f)


def load_drain3(file_path: str) -> Dict:
    with open(file_path, 'rb') as f:
        return pickle.load(f)


if __name__ == '__main__':
    from src.data.hdfs import load_data

    q = parse_file_drain3(load_data('../../data/raw/BGL/HDFS.log'))
