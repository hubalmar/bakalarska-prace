import argparse

from src.features.hdfs import create_embeddings, create_contextual_embeddings
import os

if __name__ == '__main__':
    HIDDEN = 8
    parser = argparse.ArgumentParser(description='Convert HDFS1 logs to numerical representation using fastText.')
    parser.add_argument('model', action='store', type=str, help='a path to the first trained cross-validated fastText '
                                                                'model on HDFS1')
    parser.add_argument('-in', type=str, metavar='PATH/TO/FOLDER', dest='input', default=f'../../data/interim/HDFS1/{HIDDEN}-hidden',
                        help='a location with HDFS1 training data (default: ../../data/interim/HDFS1)')
    parser.add_argument('-out', type=str, metavar='PATH/TO/FOLDER', dest='output', default='../../data/processed/HDFS1',
                        help='a location where all processed data will be saved (default: ../../data/processed/HDFS1)')
    parser.add_argument('--timedelta', action='store_true', dest='timedelta',
                        help='if provided a new feature will be generated which keeps information about time (available'
                             ' only for --per-block option)')

    group = parser.add_mutually_exclusive_group(required=True)
    group.add_argument('--per-block', dest='per_block', action='store_true', help='generate embeddings and labels per '
                                                                                  'a block of logs')
    group.add_argument('--per-log', dest='per_log', action='store_true', help='generate embeddings and labels per '
                                                                              'a log line')

    args = parser.parse_args()

    if 'fasttext' or True in args.model:
        for i in range(1,2):
            args.output = f"{args.output}/remove"
            if not os.path.exists(args.output):
                os.mkdir(args.output)
            args.output = f"{args.output}/hidden-{HIDDEN}-2nd"
            if not os.path.exists(args.output):
                os.mkdir(args.output)
            args.output = f"{args.output}/timedelta"
            if not os.path.exists(args.output):
                os.mkdir(args.output)
            args.output = f"{args.output}/{i}000"
            if not os.path.exists(args.output):
                os.mkdir(args.output)
            create_embeddings(f"{args.input}/{i}", args.output, f"{args.model}/1000fasttext-skipgram-hdfs-d100-n3-6.bin", args.per_block, args.timedelta)
    else:
        create_contextual_embeddings(args.input, args.output, args.model)
